﻿using FileResourceModule.Factories;
using FileResourceModule.Models;
using FileResourceModule.Services;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FileResourceModule
{
    public class FileResourceController : AppController
    {

        public async Task<ResultItem<FileResource>> GetFileResource(clsOntologyItem fileResource)
        {
            return await GetFileResource(fileResource, null, null);
        }
        public async Task<ResultItem<FileResource>> GetFileResource(clsOntologyItem refItem, clsOntologyItem relationType, clsOntologyItem direction)
        {
            var elasticAgent = new ElasticSearchAgent(Globals);

            var result = new ResultItem<FileResource>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new FileResource()
            };

            var resultService = await elasticAgent.GetFileResource(refItem, relationType, direction);

            if (resultService.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultService.ResultState;
                return result;
            }
            
            var factory = new FileResourceFactory();
            var resultFactory = await factory.CreateFileResource(resultService.Result, Globals);
            result.ResultState = resultFactory.ResultState;

            if (resultFactory.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            var resultServiceExclude = await elasticAgent.GetFileResource(new clsOntologyItem
            {
                GUID = resultFactory.Result.IdFileResource,
                Name = resultFactory.Result.NameFileResource,
                GUID_Parent = Config.LocalData.Class_Fileresource.GUID,
                Type = Globals.Type_Object
            }, Config.LocalData.RelationType_exclude, Globals.Direction_LeftRight);

            if (resultService.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultService.ResultState;
                return result;
            }

            var resultFactoryExclude = await factory.CreateFileResource(resultServiceExclude.Result, Globals);

            result.ResultState = resultFactoryExclude.ResultState;

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            resultFactory.Result.ExcludeFileResource = resultFactoryExclude.Result;
            result.Result = resultFactory.Result;
            return resultFactory;

        }

        public async Task<ResultItem<List<string>>> GetFilesByFileResource(FileResource fileResource)
        {
            var taskResult = await Task.Run<ResultItem<List<string>>>(async() =>
            {
                var result = new ResultItem<List<string>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<string>()
                };

                if (string.IsNullOrEmpty(fileResource.NamePath) )
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Path provided!";
                    return result;
                }
                var path = Environment.ExpandEnvironmentVariables(fileResource.NamePath);
                if (!System.IO.Directory.Exists(path))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"The Directory {path} does not exist!";
                    return result;
                }

                if (string.IsNullOrEmpty(fileResource.Pattern) )
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Pattern provided!";
                    return result;
                }

                Regex regex = null;
                try
                {
                    regex = new Regex(fileResource.Pattern);
                }
                catch (Exception ex)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Pattern {fileResource.Pattern} is not valid: {ex.Message}";
                    return result;

                }

                try
                {
                    result =  GetFiles(fileResource, path, regex);
                    
                }
                catch (Exception ex)
                {

                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Error while searching files: {ex.Message}";
                    return result;
                }

                if (fileResource.ExcludeFileResource != null)
                {
                    var getExcludedFilesResult = await GetFilesByFileResource(fileResource.ExcludeFileResource);
                    result.ResultState = getExcludedFilesResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result = (from file in result.Result
                                     join excludeFile in getExcludedFilesResult.Result on file equals excludeFile into excludedFiles
                                     from excludeFile in excludedFiles.DefaultIfEmpty()
                                     where excludeFile == null
                                     select file).ToList();
                }

                return result;
            });

            return taskResult;
        }

        private ResultItem<List<string>> GetFiles(FileResource fileResource, string path, Regex pattern)
        {
            var result = new ResultItem<List<string>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<string>()
            };

            var dateRange = fileResource?.DateRange;
            foreach (var fileItem in System.IO.Directory.GetFiles(path))
            {
                if (pattern.Match(fileItem).Success)
                {
                    if (dateRange != null)
                    {
                        var fileInfo = new FileInfo(fileItem);
                        if (fileInfo.LastWriteTime < dateRange.Start || fileInfo.LastWriteTime > dateRange.End) continue;
                    }
                    result.Result.Add(fileItem);
                }
            }

            if (fileResource.SubItems)
            {
                foreach (var subPath in System.IO.Directory.GetDirectories(path))
                {
                    var resultSubFolder = GetFiles(fileResource, subPath, pattern);

                    result.ResultState = resultSubFolder.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.AddRange(resultSubFolder.Result);
                }
            }

            return result;
        }

        public FileResourceController(Globals globals) : base(globals)
        {
        }
    }


}
