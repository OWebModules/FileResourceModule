﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileResourceModule.Services
{
    public class ElasticSearchAgent : ElasticBaseAgent
    {
        public async Task<ResultItem<FileResourceRaw>> GetFileResource(clsOntologyItem refItem, clsOntologyItem relationType, clsOntologyItem direction)
        {
            var taskResult = await Task.Run<ResultItem<FileResourceRaw>>(() =>
            {
                var result = new ResultItem<FileResourceRaw>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new FileResourceRaw
                    {
                        RefItem = refItem,
                        RelationType = relationType,
                        Direction = direction
                    }
                };
                var searchRefAndFileResource = new List<clsObjectRel>();

                if (refItem.GUID_Parent != Config.LocalData.Class_Fileresource.GUID || (refItem.GUID_Parent == Config.LocalData.Class_Fileresource.GUID && relationType != null && relationType.GUID == Config.LocalData.RelationType_exclude.GUID))
                {
                    if (direction.GUID == globals.Direction_LeftRight.GUID)
                    {
                        searchRefAndFileResource = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = refItem.GUID,
                            ID_RelationType = relationType.GUID,
                            ID_Parent_Other = Config.LocalData.Class_Fileresource.GUID
                        }
                    };
                    }
                    else
                    {
                        searchRefAndFileResource = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Other = refItem.GUID,
                            ID_RelationType = relationType.GUID,
                            ID_Parent_Object = Config.LocalData.Class_Fileresource.GUID
                        }
                    };
                    }

                    var dbReaderRefAndFileResource = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderRefAndFileResource.GetDataObjectRel(searchRefAndFileResource);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.RefAndFileResources = dbReaderRefAndFileResource.ObjectRels;

                    result.Result.FileResources = result.Result.RefAndFileResources.Select(rel => new clsOntologyItem
                    {
                        GUID = direction.GUID == globals.Direction_LeftRight.GUID ? rel.ID_Other : rel.ID_Object,
                        Name = direction.GUID == globals.Direction_LeftRight.GUID ? rel.Name_Other : rel.Name_Object,
                        GUID_Parent = direction.GUID == globals.Direction_LeftRight.GUID ? rel.ID_Parent_Other : rel.ID_Parent_Object,
                        Type = globals.Type_Object
                    }).ToList();
                }
                else
                {
                    result.Result.FileResources.Add(refItem);
                }
                

                var searchAttributes = result.Result.FileResources.Select(fileRes => new clsObjectAtt
                {
                    ID_Object = fileRes.GUID,
                    ID_AttributeType = Config.LocalData.AttributeType_Pattern.GUID
                }).ToList();

                searchAttributes.AddRange(result.Result.FileResources.Select(fileRes => new clsObjectAtt
                {
                    ID_Object = fileRes.GUID,
                    ID_AttributeType = Config.LocalData.AttributeType_Subitems.GUID
                }));

                var dbReaderAttributes = new OntologyModDBConnector(globals);

                if (searchAttributes.Any())
                {
                    result.ResultState = dbReaderAttributes.GetDataObjectAtt(searchAttributes);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.Attributes = dbReaderAttributes.ObjAtts;


                var searchFileResourceRelations = result.Result.FileResources.Select(fileRes => new clsObjectRel
                {
                    ID_Object = fileRes.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Fileresource_belonging_Resource_File.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Fileresource_belonging_Resource_File.ID_Class_Right
                }).ToList();

                searchFileResourceRelations.AddRange(result.Result.FileResources.Select(fileRes => new clsObjectRel
                {
                    ID_Object = fileRes.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Fileresource_belonging_Resource_Path.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Fileresource_belonging_Resource_Path.ID_Class_Right
                }));

                searchFileResourceRelations.AddRange(result.Result.FileResources.Select(fileRes => new clsObjectRel
                {
                    ID_Object = fileRes.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Fileresource_belonging_Resource_Web_Connection.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Fileresource_belonging_Resource_Web_Connection.ID_Class_Right
                }));

                var dbReaderRelations = new OntologyModDBConnector(globals);

                if (searchFileResourceRelations.Any())
                {
                    result.ResultState = dbReaderRelations.GetDataObjectRel(searchFileResourceRelations);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.Relations = dbReaderRelations.ObjectRels;

                return result;
            });

            return taskResult;
        }

        public ElasticSearchAgent(Globals globals) : base(globals)
        {
        }
    }

    public class FileResourceRaw
    {
        public clsOntologyItem RefItem { get; set; }
        public clsOntologyItem RelationType { get; set; }
        public clsOntologyItem Direction { get; set; }
        public List<clsObjectAtt> Attributes { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectRel> RefAndFileResources { get; set; } = new List<clsObjectRel>();
        public List<clsOntologyItem> FileResources { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> Relations { get; set; } = new List<clsObjectRel>();

    }
}
