﻿using FileResourceModule.Models;
using FileResourceModule.Services;
using LocalizationModule;
using LocalizationModule.Models;
using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileResourceModule.Factories
{
    public class FileResourceFactory
    {
        public async Task<ResultItem<FileResource>> CreateFileResource(FileResourceRaw fileResourceRaw, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<FileResource>>(async () =>
            {
                var result = new ResultItem<FileResource>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var fileResources = new List<FileResource>();

                var refIds = fileResourceRaw.FileResources.Select(res => res.GUID).ToList();

                var dateRanges = new List<DateRange>();
                if (refIds.Any())
                {
                    var localizationController = new LocalizationController(globals);
                    var getDateRangeRequest = new GetDateRangeRequest(refIds);
                    var getDateRangesResult = await localizationController.GetDateRange(getDateRangeRequest);
                    result.ResultState = getDateRangesResult.ResultState;
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                    dateRanges = getDateRangesResult.Result;
                }

                fileResources = (from fileResource in fileResourceRaw.FileResources
                                 join pattern in fileResourceRaw.Attributes.Where(att => att.ID_AttributeType == Config.LocalData.AttributeType_Pattern.GUID) on fileResource.GUID equals pattern.ID_Object
                                 join subItem in fileResourceRaw.Attributes.Where(att => att.ID_AttributeType == Config.LocalData.AttributeType_Subitems.GUID) on fileResource.GUID equals subItem.ID_Object
                                 join fileItem in fileResourceRaw.Relations.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_File.GUID) on fileResource.GUID equals fileItem.ID_Object into fileItems
                                 from fileItem in fileItems.DefaultIfEmpty()
                                 join pathItem in fileResourceRaw.Relations.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Path.GUID) on fileResource.GUID equals pathItem.ID_Object into pathItems
                                 from pathItem in pathItems.DefaultIfEmpty()
                                 join webConnectionItem in fileResourceRaw.Relations.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Web_Connection.GUID) on fileResource.GUID equals webConnectionItem.ID_Object into webConnectionItems
                                 from webConnectionItem in webConnectionItems.DefaultIfEmpty()
                                 join dateRange in dateRanges on fileResource.GUID equals dateRange.Reference.GUID into dateRanges1
                                 from dateRange in dateRanges1.DefaultIfEmpty()
                                 select new FileResource
                                 {
                                     IdDirection = fileResourceRaw.Direction?.GUID,
                                     IdRelationType = fileResourceRaw.RelationType?.GUID,
                                     NameRelationType = fileResourceRaw.RelationType?.Name,
                                     IdFileResource = fileResource.GUID,
                                     NameFileResource = fileResource.Name,
                                     IdAttributePattern = pattern.ID_Attribute,
                                     Pattern = pattern.Val_String,
                                     IdAttributeSubItems = subItem.ID_Attribute,
                                     SubItems = subItem.Val_Bool.Value,
                                     IdFile = fileItem?.ID_Other,
                                     NameFile = fileItem?.Name_Other,
                                     IdPath = pathItem?.ID_Other,
                                     NamePath = pathItem?.Name_Other,
                                     IdWebConnection = webConnectionItem?.ID_Other,
                                     NameWebconnection = webConnectionItem?.Name_Other,
                                     DateRange = dateRange
                                 }).ToList();

                result.Result = fileResources.FirstOrDefault();
                return result;
            });

            return taskResult;
        }

    }
}
