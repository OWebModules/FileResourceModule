﻿using LocalizationModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileResourceModule.Models
{
    public class FileResource
    {
        public string IdRelationType { get; set; }
        public string NameRelationType { get; set; }
        public string IdDirection { get; set; }

        public string IdFileResource { get; set; }
        public string NameFileResource { get; set; }

        public string IdAttributePattern { get; set; }
        public string Pattern { get; set; }

        public string IdAttributeSubItems { get; set; }
        public bool SubItems { get; set; }

        public string IdFile { get; set; }
        public string NameFile { get; set; }
        
        public string IdPath { get; set; }
        public string NamePath { get; set; }

        public string IdWebConnection { get; set; }
        public string NameWebconnection { get; set; }

        public DateRange DateRange { get; set; }

        public FileResource ExcludeFileResource { get; set; }
    }
}
